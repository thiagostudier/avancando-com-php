<?php

$conn = require __DIR__ . '/utils/connection.php';

$conn->query('TRUNCATE posts');

$save = true;

$sql = file_get_contents(__DIR__ . '/insert_posts.sql');

$conn->begin_transaction(); //TRANSACTION
$conn->query($sql);
// $conn->commit(); //TRANSACTION
// $conn->rollback(); //TRANSACTION

if($save){
    $conn->commit();
}else{
    $conn->rollback();
}

echo 'START SELECT' . PHP_EOL;

$result = $conn->query('SELECT * FROM posts');

$posts = $result->fetch_all(MYSQLI_ASSOC);

foreach($posts as $post){
    echo $post['id']. PHP_EOL ;
    echo $post['title']. PHP_EOL ;
    echo $post['body']. PHP_EOL ;
}

echo 'END SELECT' . PHP_EOL;