<?php

$conn = require __DIR__ . '/utils/connection.php';

!$conn->query('TRUNCATE comments');

$save = true;

$sql = file_get_contents(__DIR__ . '/insert_comments.sql');

$conn->begin_transaction(); //TRANSACTION
$conn->query($sql);
// $conn->commit(); //TRANSACTION
// $conn->rollback(); //TRANSACTION

if($save){
    $conn->commit();
}else{
    $conn->rollback();
}

echo 'START SELECT' . PHP_EOL;

$result = $conn->query('SELECT * FROM comments');

$comments = $result->fetch_all(MYSQLI_ASSOC);

foreach($comments as $comment){
    echo $comment['email']. PHP_EOL ;
    echo $comment['comment']. PHP_EOL ;
}

echo 'END SELECT' . PHP_EOL;