<?php

$debug = true;

// MYSQLI_REPORT_ERROR
// MYSQLI_REPORT_OFF
// MYSQLI_REPORT_STRICT
// MYSQLI_REPORT_INDEX
// MYSQLI_REPORT_ALL

if($debug){
    mysqli_report(MYSQLI_REPORT_ERROR);
}else{
    mysqli_report(MYSQLI_REPORT_OFF);
}

$conn = new mysqli('localhost', 'root', '', 'avancando_php_mysql');

if($conn->connect_errno){
    die('Falou em conectar');
}

return $conn;